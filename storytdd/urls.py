"""storytdd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.urls import re_path
from django.contrib import admin
import story6.urls as story6
from .views import logoutPage
from django.contrib.auth import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^story6/',include(('story6.urls', 'story6'), namespace = 'story6')),
	url(r'^story9/',include(('story9.urls', 'story9'), namespace = 'story9')),
	url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^logout/',logoutPage, name='logout'),
]

