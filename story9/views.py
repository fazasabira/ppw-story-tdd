from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from .forms import SearchForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
import requests
import json


import requests
import json
#Create your views here.
def search_book(request):
    response = {}
    response['form'] = SearchForm()

    response['data'] = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            search = cd['title']
            URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
            get_json = requests.get(URL).json()
            json_dict = json.dumps(get_json)
            response['data'] = json_dict

        return render(request, "listbook.html", response)
    else:
        return render(request, "listbook.html", response)

def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):

	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")




# def logout_view(request):
#     logout(request)
#     return HttpResponseRedirect('/books')


