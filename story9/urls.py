from django.urls import path, include
from .views import *
from django.contrib import admin 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings


app_name = "story9"
urlpatterns = [
    path('', search_book, name="listbook"),
    path('tambah', tambah, name="tambah"),
	path('kurang', kurang, name="kurang"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

