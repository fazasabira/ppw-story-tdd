from django.test import TestCase
from django.test import Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story6UnitTest(TestCase):
    def test_story6_page_has_hello(self):
        response = Client().get('/story6/tdd/')
        landing_page_content = 'Hello, Apa kabar?'
        html_response = response.content.decode('utf8')
        self.assertIn(landing_page_content,html_response)

    def test_story6_url_is_exist(self):
        response = Client().get('/story6/tdd/')
        self.assertEqual(response.status_code,200)

    def test_story6_post_is_exist(self):
        response = Client().post('/story6/tdd/')
        self.assertEqual(response.status_code,200)

    def test_story6_page_has_name(self):
        response = Client().get('/story6/tdd/')
        profile_page_content = 'Faza Siti Sabira Prakoso'
        html_response = response.content.decode('utf8')
        self.assertIn(profile_page_content,html_response)


class Story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://tdd-ppw-faza.herokuapp.com/story6/tdd/')
        # find the form element
        title = selenium.find_element_by_id('id_status') 
        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        title.send_keys('Coba Coba')
         
        # submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn('Coba Coba', selenium.page_source)
    
    def test_background_is_powderblue(self):
        selenium = self.selenium
        selenium.get('https://tdd-ppw-faza.herokuapp.com/story6/tdd/')
        body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertEqual(body,"rgba(176, 224, 230, 1)")
    
    def test_layout_profile(self):
        selenium = self.selenium
        selenium.get('https://tdd-ppw-faza.herokuapp.com/story6/tdd/')
        header_text = selenium.find_element_by_tag_name('h1').text
        self.assertIn('Hello, Apa kabar?', header_text)

    def test_input_status_box_location(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://tdd-ppw-faza.herokuapp.com/story6/tdd/')
        statusbox = selenium.find_element_by_tag_name('form')

    def test_input_submit_button_location(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://tdd-ppw-faza.herokuapp.com/story6/tdd/')
        submit = selenium.find_element_by_tag_name('button')
        





    



    