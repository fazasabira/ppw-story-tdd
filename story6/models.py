from django.db import models
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    date = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=300)
