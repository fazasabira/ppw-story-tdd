from django.shortcuts import render
from .forms import StatusForm
from .models import Status

# Create your views here.
def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            new_status = form.save()
            new_status.save()
    form = StatusForm()
    statusss = Status.objects.all().values()
    return render(request,'hw.html', {'form': form, 'statuss': statusss})