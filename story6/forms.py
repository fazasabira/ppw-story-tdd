from django import forms

from .models import Status

# class Date(forms.DateTimeInput):
#     input_type = 'datetime-local'

class StatusForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ('status',)